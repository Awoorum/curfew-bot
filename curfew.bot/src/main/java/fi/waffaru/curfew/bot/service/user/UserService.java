package fi.waffaru.curfew.bot.service.user;

import net.dv8tion.jda.api.entities.User;

public interface UserService {

    boolean persist(User user);
}
