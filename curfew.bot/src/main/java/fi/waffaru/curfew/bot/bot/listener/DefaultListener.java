package fi.waffaru.curfew.bot.bot.listener;

import fi.waffaru.curfew.bot.bot.BotConfiguration;
import fi.waffaru.curfew.bot.service.user.UserService;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.EventListener;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties(BotConfiguration.class)
public class DefaultListener implements EventListener {

    private final String prefix;
    private final UserService userService;

    @Autowired
    public DefaultListener(final BotConfiguration botConfiguration, final UserService userService) {
        this.prefix = botConfiguration.getPrefix();
        this.userService = userService;
    }

    @Override
    public void onEvent(@NotNull GenericEvent event) {
        if(event instanceof ReadyEvent)
            System.out.println("API has loaded");
        if(event instanceof MessageReceivedEvent) {
            MessageReceivedEvent messageReceivedEvent = (MessageReceivedEvent) event;
            if(((MessageReceivedEvent) event).getAuthor().isBot())
                return;
            String message = messageReceivedEvent.getMessage().getContentRaw();
            if(message.contains(prefix+"ping")) {
                MessageChannel channel = messageReceivedEvent.getChannel();
                channel.sendMessage(messageReceivedEvent.getAuthor().getId() + " Pong!").queue();
            }
            if(message.contains(prefix+"persist")) {
                MessageChannel channel = messageReceivedEvent.getChannel();
                channel.sendMessage("Persisting user...").queue();
                if(userService.persist(messageReceivedEvent.getAuthor())) {
                    channel.sendMessage("User with name: " + messageReceivedEvent.getAuthor().getName() +
                            " and ID: " + messageReceivedEvent.getAuthor().getId() + " persisted to database").queue();
                }
            }
            if(message.contains(prefix+"timezone")) {
                MessageChannel channel = messageReceivedEvent.getChannel();
                if(message.contains("help")) {
                    channel.sendMessage(prefix+"timeZone {TIMEZONE} {START_TIME} {DURATION_HOURS}").queue();
                    channel.sendMessage("Example: " +prefix+"timezone Europe/Helsinki 22:30 6H").queue();
                    channel.sendMessage("Please check your timezone from this table:" +
                            " https://garygregory.wordpress.com/2013/06/18/what-are-the-java-timezone-ids/" ).queue();
                }
                else {
                    channel.sendMessage("Invalid input, type " + prefix + "timezone help for more information").queue();
                }
            }
        }
    }

}
