package fi.waffaru.curfew.bot.entity;

import fi.waffaru.curfew.bot.entity.base.UUIDEntity;
import lombok.Data;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * Entity class representing a Curfew row in curfew table
 */

@Data
@Entity
@Table(name = "curfew")
public class Curfew extends UUIDEntity {
}
