package fi.waffaru.curfew.bot.service.curfew;

import fi.waffaru.curfew.bot.repository.CurfewRepository;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CurfewServiceImpl implements CurfewService {

    private CurfewRepository curfewRepository;

    @Autowired
    public CurfewServiceImpl(final CurfewRepository curfewRepository) {
        this.curfewRepository = curfewRepository;
    }
}
