package fi.waffaru.curfew.bot.entity;

import fi.waffaru.curfew.bot.entity.base.UUIDEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Entity class representing an User in user table
 */

@Data
@Entity
@Table(name = "user", schema = "public")
public class User extends UUIDEntity {
    @Column(name = "discord_id")
    public String discordId;
}
