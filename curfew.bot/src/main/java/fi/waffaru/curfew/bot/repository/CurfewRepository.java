package fi.waffaru.curfew.bot.repository;

import fi.waffaru.curfew.bot.entity.Curfew;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurfewRepository extends JpaRepository<Curfew, Long> {
}
