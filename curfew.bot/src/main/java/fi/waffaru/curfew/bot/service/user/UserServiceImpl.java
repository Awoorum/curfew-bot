package fi.waffaru.curfew.bot.service.user;

import fi.waffaru.curfew.bot.repository.UserRepository;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public boolean persist(User user) {
        fi.waffaru.curfew.bot.entity.User s = new fi.waffaru.curfew.bot.entity.User();
        s.setDiscordId(user.getId());
        s.setId(UUID.randomUUID());
        userRepository.saveAndFlush(s);
        return true;
    }
}
