package fi.waffaru.curfew.bot.bot;


import fi.waffaru.curfew.bot.bot.listener.DefaultListener;
import lombok.extern.log4j.Log4j2;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.security.auth.login.LoginException;

@Component
@EnableConfigurationProperties(BotConfiguration.class)
@Log4j2
public class Bot {
    JDA jda;

    @Autowired
    public Bot(final BotConfiguration botConfiguration, final DefaultListener defaultListener) throws LoginException {
        log.info("Initializing bot");
        jda = JDABuilder.createDefault(botConfiguration.getToken())
                .addEventListeners(defaultListener)
                .build();
    }
}
