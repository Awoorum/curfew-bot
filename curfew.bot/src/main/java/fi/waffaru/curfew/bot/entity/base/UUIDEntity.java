package fi.waffaru.curfew.bot.entity.base;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@Data
@Embeddable
@MappedSuperclass
public class UUIDEntity {
    @Id
    private UUID id;
}
